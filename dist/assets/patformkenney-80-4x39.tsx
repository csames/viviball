<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.5" tiledversion="1.6.0" name="patformkenney-80-4x39" tilewidth="80" tileheight="80" tilecount="156" columns="4">
 <image source="patformkenney-80-4x39.png" width="320" height="3120"/>
 <tile id="148">
  <properties>
   <property name="collides" type="bool" value="true"/>
   <property name="ground" type="bool" value="true"/>
  </properties>
 </tile>
</tileset>
