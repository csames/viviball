import Phaser from 'phaser';
import { events, EventType } from './EventCenter';

type CursorKeys = Phaser.Types.Input.Keyboard.CursorKeys;
type CollisionData = Phaser.Types.Physics.Matter.MatterCollisionData;

export default class PlayerController {
    private scene: Phaser.Scene;
    private sprite: Phaser.Physics.Matter.Sprite;
    private cursors: CursorKeys;
    private ground: Phaser.Tilemaps.TilemapLayer;

    private speed = 5;
    private burgersCollected = 0;
    private lives = 2;
    private onGround = false;
    private dead = false;

    constructor(
      scene: Phaser.Scene,
      sprite: Phaser.Physics.Matter.Sprite,
      cursors: CursorKeys,
      ground: Phaser.Tilemaps.TilemapLayer
    ) {
        this.scene = scene;
        this.sprite = sprite;
        this.ground = ground;
        this.cursors = cursors;

        const radius = this.sprite.width / 2;
        this.sprite.setCircle(radius);

        this.sprite.setScale(0.5);

        this.sprite.setOnCollide((data: CollisionData) => {
            const body = data.bodyA as MatterJS.BodyType;

            const gameObject = body.gameObject;

            if (!gameObject || this.dead) {
                return;
            }

            if (gameObject instanceof Phaser.Physics.Matter.TileBody) {
                if (gameObject.tile.properties.ground) {
                    this.onGround = true;
                }
                return;
            }

            const sprite = gameObject as Phaser.Physics.Matter.Sprite;

            const type = sprite.getData('type');

            switch (type) {
                case 'burger': {
                    events.emit(EventType.BURGER_EATEN);
                    sprite.destroy();
                    this.burgersCollected++;
                    break;
                }
                case 'spring': {
                    if (!this.onGround || this.sprite.body.velocity.y > 0.5) {
                        events.emit(EventType.SPRING_JUMP);
                        this.jump();
                    }
                    break;
                }
                case 'spike': {
                    events.emit(EventType.SPIKE_TOUCHED);
                    this.lives--;
                    this.sprite.setVelocityY(-2 * this.speed);
                    this.dead = true;
                    this.sprite.setVelocityX(0);

                    setTimeout(() => {
                        this.sprite.setY(200)
                        this.sprite.setX(400)
                        document.location.reload()
                    }, 900);
                    break;
                }
                case 'teleport': {
                    alert('Szép munka! Még egyszer?')
                    document.location.reload()
                }
            }
        });

        this.scene.cameras.main.startFollow(this.sprite);
    }

    update(dt: number) {
        if (this.dead) {
            return;
        }
        if (this.cursors.left.isDown) {
            this.sprite.setVelocityX(-this.speed);
        } else if (this.cursors.right.isDown) {
            this.sprite.setVelocityX(this.speed);
        } else {
            this.sprite.setVelocityX(0);
        }

        if (this.onGround && this.cursors.up.isDown) {
            this.sprite.setVelocityY(-2 * this.speed);
            this.onGround = false;
        }
    }

    jump() {
        this.sprite.setVelocityY(-3 * this.speed);
    }
}
