import Phaser from 'phaser';
import Game from './scenes/Game';

import UI from './scenes/UI';

const config: Phaser.Types.Core.GameConfig = {
    type: Phaser.AUTO,
    scale: {
        mode: Phaser.Scale.FIT,
        autoCenter: Phaser.Scale.CENTER_BOTH,
    },
    physics: {
        default: 'matter',
        matter: {
            debug: true,
        },
    },
    scene: [Game, UI],
};

export default new Phaser.Game(config);
