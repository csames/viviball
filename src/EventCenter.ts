import Phaser from 'phaser';

export const EventType = {
    SPRING_JUMP: 'spring-jump',
    BURGER_EATEN: 'burger-eaten',
    SPIKE_TOUCHED: 'spike-touched',
};

export const events = new Phaser.Events.EventEmitter();
