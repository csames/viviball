import Phaser from 'phaser';
import { events, EventType } from '~/EventCenter';
import PlayerController from '~/PlayerController';

export default class Game extends Phaser.Scene {
    private ball!: PlayerController;
    private cursors!: Phaser.Types.Input.Keyboard.CursorKeys;

    constructor() {
        super('game');
    }

    init() {
        this.cursors = this.input.keyboard.createCursorKeys();

        this.events.on(Phaser.Scenes.Events.SHUTDOWN, () => {
            this.scene.stop('ui');
        });
    }

    preload() {
        this.load.image('ball', 'assets/ball.png');
        this.load.image('burger', 'assets/burger.png');
        this.load.image('spike', 'assets/spikes.png');
        this.load.image('teleport', 'assets/doar.png');
        this.load.spritesheet(
            'spritesheet',
            'assets/patformkenney-80-4x39.png',
            { frameWidth: 80, frameHeight: 80 }
        );

        for (let i = 0; i < 8; ++i) {
            this.load.image(
                `spring${i}`,
                `assets/animations/spring/spring-${i}.png`
            );
        }

        this.load.image('tiles', 'assets/patformkenney-80-4x39.png');
        this.load.tilemapTiledJSON('tilemap', 'assets/map.json');
    }

    create() {
        this.scene.launch('ui');

        this.createAnims();
        const map = this.make.tilemap({ key: 'tilemap' });
        const tileset = map.addTilesetImage(
            'patformkenney-80-4x39',
            'tiles'
        );

        const ground = map.createLayer('ground', tileset);
        ground.setCollisionByProperty({ collides: true });
        this.cameras.main.setBackgroundColor('#C99869')
        this.matter.world.convertTilemapLayer(ground);

        const objectsLayer = map.getObjectLayer('objects');
        objectsLayer.objects.forEach(objData => {
            const { x = 0, y = 0, width = 0, height = 0, name } = objData;
            switch (name) {
                case 'burger': {
                    const burger = this.matter.add.sprite(
                        x + width / 2,
                        y + height / 2,
                        'burger',
                        undefined,
                        {
                            isStatic: true,
                            isSensor: true,
                        }
                    );
                    burger.setData('type', 'burger');
                    break;
                }
                case 'spring': {
                    const spring = this.matter.add.sprite(
                        x + width / 2,
                        y + height / 2,
                        'spring7',
                        undefined,
                        {
                            isStatic: true,
                            isSensor: true,
                        }
                    );
                    events.on(EventType.SPRING_JUMP, () =>
                        spring.play('bounce')
                    );
                    spring.setData('type', 'spring');
                    break;
                }
                case 'spike': {
                    const spike = this.matter.add.sprite(
                        x + width / 2,
                        y + height / 2,
                        'spike',
                        31,
                        {
                            isStatic: true,
                            isSensor: true,
                        }
                    );
                    spike.setData('type', 'spike');
                    break;
                }
                case 'teleport': {
                    const teleport = this.matter.add.sprite(
                      x + width / 2,
                      y + height / 2,
                      'teleport',
                      undefined,
                      {
                          isStatic: true,
                          isSensor: true,
                      }
                    );
                    teleport.setData('type', 'teleport');
                    break;
                }
            }
        });

        const { width, height } = this.scale;

        const ballSprite = this.matter.add.sprite(
            width / 2,
            height / 3,
            'ball'
        );
        ballSprite.setBounce(0.5);

        this.ball = new PlayerController(this, ballSprite, this.cursors, ground);
    }

    update(t: number, dt: number) {
        this.ball.update(dt);
    }

    createAnims() {
        const bounceFrames = new Array<object>();
        for (let i = 7; i >= 0; --i) {
            bounceFrames.push({ key: `spring${i}` });
        }
        this.anims.create({
            key: 'bounce',
            frames: bounceFrames,
            repeat: 0,
            frameRate: 60,
            yoyo: true,
        });
    }
}
