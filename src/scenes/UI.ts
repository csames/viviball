import Phaser from 'phaser';
import { events, EventType } from '~/EventCenter';

export default class UI extends Phaser.Scene {
    private burgersEaten = 0;
    private lives = 2;
    private burgersLabel!: Phaser.GameObjects.Text;
    private livesLabel!: Phaser.GameObjects.Text;

    constructor() {
        super('ui');
    }

    init() {
        this.burgersEaten = 0;
        this.lives = 2;
    }

    create() {
        this.burgersLabel = this.add.text(
            10,
            10,
            `Burgers: ${this.burgersEaten}`,
            {
                fontSize: '32px',
            }
        );

        this.livesLabel = this.add.text(10, 44, `Lives: ${this.lives}`, {
            fontSize: '32px',
        });

        events.on(EventType.BURGER_EATEN, context => {
            this.burgersLabel.setText(`Burgers: ${++this.burgersEaten}`);
        });

        events.on(EventType.SPIKE_TOUCHED, context => {
            this.livesLabel.setText(`Lives: ${--this.lives}`);
        });

        this.events.once(Phaser.Scenes.Events.SHUTDOWN, () => {
            events.off(EventType.BURGER_EATEN);
            events.off(EventType.SPIKE_TOUCHED);
        });
    }
}
